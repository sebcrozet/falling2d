#ifndef FALLING2D_MATRIX2D
# define FALLING2D_MATRIX2D

# include <Falling/Algebra/OrthogonalMatrix.hh>
# include <Falling/Algebra/Rotatable.hh>

namespace Falling
{
  namespace Algebra
  {
    template <>
      struct RotatableTrait<OrthogonalMatrix<2>>
      {
        static const bool value = true;

        typedef Vect<1> RotationType;

        static Vect<1> rotation(const OrthogonalMatrix<2>& mat)
        {
          Vect<1> res;

          res.components[0] = -atan(mat.rotation()[0] / mat.rotation()[1]);

          return res;
        }

        static OrthogonalMatrix<2>& rotate(const Vect<1>& ang, OrthogonalMatrix<2>& mat)
        {
          double              siteta = sin(ang.components[0]);
          double              coteta = cos(ang.components[0]);
          OrthogonalMatrix<2> rotmat;
          rotmat.rotation()[0] = coteta;
          rotmat.rotation()[1] = -siteta;
          rotmat.rotation()[2] = siteta;
          rotmat.rotation()[3] = coteta;

          mat = mat * rotmat;

          return mat;
        }
      };
  } // end Matrix2d
} // end Falling2d

#endif // FALLING2D_MATRIX2D
