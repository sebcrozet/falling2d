#ifndef FALLING2D_VECTOR2D_HH
# define FALLING2D_VECTOR2D_HH

# include <iostream>

# include <Falling/Algebra/CrossProd.hh>
# include <Falling/Algebra/Vect.hh>

namespace Falling
{
  namespace Algebra
  {
    template <>
      struct CrossProdTrait<Vect<2>>
      {
        static const bool value = true;

        typedef Vect<1> ResultType;

        static Vect<1> cross(const Vect<2>& a, const Vect<2>& b)
        {
          Vect<1> res;

          res.components[0] = a.components[0] * b.components[1]
                              - a.components[1] * b.components[0];

          return res;
        }
      };
  } // end Algebra
} // end Falling
#endif
