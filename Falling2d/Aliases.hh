#ifndef FALLING2D_ALIASES
# define FALLING2D_ALIASES

# include <Falling/Metal/Inherit.hh>
# include <Falling/Metal/Sort.hh>

# include <Falling/Algebra/OrthogonalMatrix.hh>
# include <Falling/Algebra/Vect.hh>
# include <Falling/Algebra/Error.hh>

# include <Falling/Collision/Contact.hh>
# include <Falling/Collision/Detection/BroadPhase/BruteForceBroadPhase.hh>
# include <Falling/Collision/Detection/BroadPhase/BruteForceLoozeBoundingVolumeBroadPhase.hh>
# include <Falling/Collision/Detection/BroadPhase/DynamicLoozeBoundingVolumeHierarchyBroadPhase.hh>
# include <Falling/Collision/Detection/NarrowPhase/AbstractNarrowPhase.hh>
# include <Falling/Collision/Detection/NarrowPhase/DefaultCollisionDispatcher.hh>
# include <Falling/Collision/Detection/NarrowPhase/JohnsonSimplex.hh>
# include <Falling/Collision/Detection/NarrowPhase/GJK.hh>
# include <Falling/Collision/Graph/AllNarrowPhaseAccumulator.hh>

# include <Falling/Constraint/Solver/AccumulatedImpulseConstraintSolver.hh>
# include <Falling/Constraint/Solver/IndexProxy.hh>

# include <Falling/Integrator/BodyGravityIntegrator.hh>

# include <Falling/Body/RigidBodyIntegrator.hh>
# include <Falling/Body/RigidBody.hh>
# include <Falling/Body/RigidBodyCollisionDispatcher.hh>
# include <Falling/Body/RigidBodyIndexer.hh>

# include <Falling/Shape/ImplicitShape/HasBoundingVolumeAABB.hh>

# include <Falling/Shape/Ball/Ball.hh>
# include <Falling/Shape/Ball/ImplicitShape.hh>
# include <Falling/Shape/Ball/TransformableShape.hh>
# include <Falling/Shape/Ball/HasBoundingVolumeAABB.hh>
# include <Falling/Shape/Ball/HasCenterOfMass.hh>

# include <Falling/Shape/Plane/Plane.hh>
# include <Falling/Shape/Plane/TransformableShape.hh>
# include <Falling/Shape/Plane/HasBoundingVolumeAABB.hh>

# include <Falling/Shape/Box/Box.hh>
# include <Falling/Shape/Box/ImplicitShape.hh>
# include <Falling/Shape/Box/TransformableShape.hh>
# include <Falling/Shape/Box/HasCenterOfMass.hh>

# include <Falling/Shape/TransformedShape/TransformedShape.hh>
# include <Falling/Shape/TransformedShape/ImplicitShape.hh>
# include <Falling/Shape/TransformedShape/TransformableShape.hh>
# include <Falling/Shape/TransformedShape/HasCenterOfMass.hh>
# include <Falling/Shape/TransformedShape/VolumetricShape.hh>

# include <Falling/Shape/CSO/ImplicitShape.hh>
# include <Falling/Shape/CSO/HasCenterOfMass.hh>

# include <Falling/BoundingVolume/AABB/HasCenterOfMass.hh>
# include <Falling/BoundingVolume/AABB/BoundingVolume.hh>
# include <Falling/BoundingVolume/AABB/LoozeBoundingVolume.hh>

# include <Falling/World/GenericWorld.hh>

# include <Falling2d/Shape2d.hh>
# include <Falling2d/Vector2d.hh>
# include <Falling2d/Vector1d.hh>
# include <Falling2d/Matrix2d.hh>

namespace Falling2d
{
  using namespace Falling;
  using namespace Falling::Algebra;
  using namespace Falling::Integrator;
  using namespace Falling::Body;
  using namespace Falling::World;
  using namespace Falling::Shape;
  using namespace Falling::Collision;
  using namespace Falling::Metal;
  using namespace Falling::Collision::Detection::BroadPhase;
  using namespace Falling::Collision::Detection::NarrowPhase;
  using namespace Falling::Constraint::Solver;


  using Vect2d      = Vect<2>;
  using Vect1d      = Vect<1>;
  using BoundingVolume2d = AABB<Vect2d>;
  using Transform2d = OrthogonalMatrix<2>;
  using Shape2d     = InheritLattice<
                        Metal::Dynamic::Identifiable
                        , BoundingVolume::Dynamic::HasBoundingVolume<BoundingVolume2d>
                      >;

  using Body2d = RigidBody<Shape2d
                           , Transform2d
                           , double
                           , double
                           , IndexProxy
                           // , LoozeBoundingVolumeProxy<BoundingVolume2d> // FIXME: remove that
                           , BVHTreeNodeProxy<BoundingVolume2d>>;

  template <typename T> 
    using PartialBodyIntegrator = BodyGravityIntegrator<T, Vect2d, Vect1d>;

  using BodyGravityIntegrator2d      = BodyGravityIntegrator<Body2d, Vect2d, Vect1d>;
  using RigidBodyGravityIntegrator2d = RigidBodyIntegrator<PartialBodyIntegrator, Body2d>;
  using BroadPhase2d           = DynamicLoozeBoundingVolumeHierarchyBroadPhase<Body2d, BoundingVolume2d>;
                                 // BruteForceLoozeBoundingVolumeBroadPhase<Body2d, BoundingVolume2d>;
                                 // BruteForceBroadPhase<Body2d>;
  using GJKSimplex = JohnsonSimplex<AnnotatedSupportPoint<Vect2d>>;

  using SubCollisionDispatcher2d = 
    DefaultCollisionDispatcher<Vect2d, Transform2d, GJKSimplex, Error<double>, 25>;

  using CollisionDispatcher2d = RigidBodyCollisionDispatcher<SubCollisionDispatcher2d, Body2d>;

  using NarrowPhase2d = AbstractNarrowPhase<
                          Contact<Vect2d>
                          , Metal::ConstInheritLattice<Metal::Dynamic::Identifiable>::type
                        >;
  using Contact2d   = Contact<Vect2d>;
  using Accumulator2d = AllNarrowPhaseAccumulator<
                          Body2d
                          , NarrowPhase2d
                          , Contact2d
                          , Metal::ConstInheritLattice<Metal::Dynamic::Identifiable>::type>;

  using Ball2d  = Ball<Vect2d>;
  using Box2d   = Box<Vect2d>;
  using Plane2d = Plane<Vect2d>;

  using ConstraintsSolver2d = AccumulatedImpulseConstraintSolver<Body2d,
                                                                 Vect2d,
                                                                 double,
                                                                 RigidBodyIndexer<Body2d>>;

  using World2d = GenericWorld<Body2d
                               , RigidBodyGravityIntegrator2d
                               , NarrowPhase2d
                               , BroadPhase2d
                               , CollisionDispatcher2d
                               , Accumulator2d
                               , ConstraintsSolver2d>;
}

#endif // FALLING2D_ALIASES
