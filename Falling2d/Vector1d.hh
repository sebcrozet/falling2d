#ifndef FALLING2D_VECTOR1D
# define FALLING2D_VECTOR1D

namespace Falling
{
  namespace Algebra
  {
    template <>
      struct DimensionTrait<double>
      {
        static const bool     value = true;
        static const unsigned dim   = 1;
      };
  } // end Algebra
} // end Falling

#endif // FALLING2D_VECTOR1D
