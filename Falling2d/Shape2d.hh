#ifndef FALLING2D_SHAPE2D
# define FALLING2D_SHAPE2D

# include <Falling/Shape/VolumetricShape.hh>
# include <Falling/Algebra/OrthogonalMatrix.hh>
# include <Falling/Shape/Ball/Ball.hh>

namespace Falling
{
  namespace Shape
  {
    template <>
      struct VolumetricShapeTrait<Ball<Vect<2u>>>
      {
        static const bool value = true;

        using InertiaTensorType = double;

        static double volume(const Ball<Vect<2u>>& ball)
        {
          return M_PI * ball.radius() * ball.radius();
        }

        static InertiaTensorType inertiaTensor(const Ball<Vect<2u>>& ball, double mass)
        {
          // FIXME: take the center in account?
          return mass * ball.radius() * ball.radius() / 2.0;
        }
      };

    template <>
      struct VolumetricShapeTrait<Box<Vect<2u>>>
      {
        static const bool value = true;

        using InertiaTensorType = double;

        static double volume(const Box<Vect<2u>>& box)
        {
          return 2.0 * box.halfExtents()[0] * 2.0 * box.halfExtents()[1];
        }

        static InertiaTensorType inertiaTensor(const Box<Vect<2u>>& box, double mass)
        {
          return mass * sqlen(box.halfExtents()) / 12.0;
        }
      };
  }
}

#endif // FALLING2D_SHAPE2D
